# Ausreißererkennung mithilfe Dimensionsreduktionstechniken
>  *"So we fix our eyes not on what is seen, but on what is unseen, since what is seen is temporary, but what is unseen is eternal."*

## Notwendige Einstellung
Also erstmal muss man die Umgebungvariable (`DATA_DIR`) für den Ordnerpfad der Datensätzen einstellen, damit die 
Programmen ausführbar sind. Die lesbare Datensätze sind nur in `.mat`-Format.

## Docker Einstellungen
```shell
docker build -t oddr .
docker run --rm -it -e DATA_DIR=/usr/src/app/dataset oddr execute.py <detector> <reducer> <max_eval> <upload> <file_id> <data>
```

## Beschreibung
Du würdest in deiner Bachelorarbeit untersuchen, ob man bessere Ausreißer-Ergebnisse erzielen kann durch die Verwendung 
von unsupervised(unüberwachten) Dimensionsreduktionstechniken.

Folgende Aufgaben erwarten dich konkret:
- Fasse dich mit dem State of Art von unsupervised Dimensionsreduktionstechniken auseinander (sowohl klassische als 
  auch neuere)
- Überlege dir wieso es sinnvoll sein kann, die Dimensionen der Daten zu reduzieren.
- Wähle geeignete Ausreißer-Algorithmen (LOF, KNN, Isolation forest,  INNE, COPD,  COF, ABOD usw.)
- Wähle geeignete Datensätze und überlege dir Experimente, um die Ausgangsfrage zu beantworten.
- Zusammenfassung der Ergebnisse.

## Pipeline
- Raw Data 
- Dimensionsreduktion Technik anwenden 
- Auf diese Features Ausreißer-Verfahren anwenden 
- Vergleich der Ergebnisse mit Raw Data Ausreißererkennung.
