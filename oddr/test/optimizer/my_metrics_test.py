import numpy as np
from oddr.main.measurer import my_metrics


# Check Validity
# Run this in IDE
if __name__ == "__main__":
    # TP = 9, TN = 4, FP= 3, FN=2
    y_true = np.array(
        [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1, -1]]
    ).transpose()
    print(y_true)
    y_pred = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1, 1, 1, 1])

    precision = 9 / 12
    recall = 9 / 11
    accuracy = 13 / 18
    f1 = 2 * ((recall * precision) / (recall + precision))
    p0 = accuracy
    pe = 12 / 18 * 11 / 18 + 6 / 18 * 7 / 18
    cohen_kappa = (p0 - pe) / (1 - pe)
    mcc = (9 * 4 - 3 * 2) / ((9 + 3) * (9 + 2) * (4 + 3) * (4 + 2)) ** 0.5

    print(my_metrics.precision(y_true, y_pred))
    print(my_metrics.precision(y_true, y_pred) == precision)
    print(my_metrics.recall(y_true, y_pred))
    print(my_metrics.recall(y_true, y_pred) == recall)
    print(my_metrics.accuracy(y_true, y_pred))
    print(my_metrics.accuracy(y_true, y_pred) == accuracy)
    print(my_metrics.f1(y_true, y_pred))
    print(my_metrics.f1(y_true, y_pred) == f1)
    print(my_metrics.cohen_kappa(y_true, y_pred))
    print(my_metrics.cohen_kappa(y_true, y_pred) == cohen_kappa)
    print(my_metrics.matthew_correlation(y_true, y_pred))
    print(my_metrics.matthew_correlation(y_true, y_pred) == mcc)
    print(my_metrics.all(y_true, y_pred))
