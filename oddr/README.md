### Dimensionality Reduction
- PCA
- tSNE
- UMAP
- Autoencoder

### Outlier Detection:
- Proximity based - LOF
- Proximity based - COF
- Proximity based - kNN
- Outlier Ensembles - IForest
- Outlier Ensembles- INNE
- Probabilistic - COPOD
- Probabilistic - ABOD

### Should we implement genetic algorithm and simulated annealing?

```python
# Simulated Annealing?
pyod.models.cof.COF(contamination=0.1, n_neighbors=20)
pyod.models.abod.ABOD(contamination=0.1, n_neighbors=5, method='fast')

# Parameter free
pyod.models.copod.COPOD(contamination=0.1)

# Genetic Algorithm?
pyod.models.iforest.IForest(n_estimators=100, max_samples='auto', 
                            contamination=0.1, max_features=1.0, 
                            bootstrap=False, n_jobs=1, 
                            behaviour='old', random_state=None, 
                            verbose=0)
pyod.models.knn.KNN(contamination=0.1, n_neighbors=5, 
                    method='largest', radius=1.0, 
                    algorithm='auto', leaf_size=30, 
                    metric='minkowski', p=2, 
                    metric_params=None, n_jobs=1)
pyod.models.lof.LOF(n_neighbors=20, algorithm='auto', 
                    leaf_size=30, metric='minkowski', 
                    p=2, metric_params=None, 
                    contamination=0.1, n_jobs=1)
anomatools.models.inne.iNNE(n_members=100,sample_size=16,
                 contamination=0.1,metric='euclidean',
                 tol=1e-8,verbose=False)
```