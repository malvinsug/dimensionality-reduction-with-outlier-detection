from pyod.models.abod import ABOD
from pyod.models.cof import COF
from pyod.models.copod import COPOD
from pyod.models.iforest import IForest
from pyod.models.knn import KNN
from pyod.models.lof import LOF
from anomatools.models.inne import iNNE

from sklearn.decomposition import PCA
from openTSNE import TSNE
from umap import UMAP


from math import ceil

from oddr.main.measurer import my_metrics
from oddr.main.labs.reporter import LocalReporter
from oddr.main.labs.hyperparameter_manager import *
from oddr.main.utils.converter import convert_y_to_sckit, prepare_y_true

from hyperopt import fmin, space_eval, tpe
from datetime import datetime, timedelta

from scipy.io import loadmat
from sklearn.metrics import confusion_matrix
import numpy as np


def fit_and_calculate_obj_result(detector, X_data, y_true):
    detector.fit(X_data)
    y_pred = convert_y_to_sckit(detector.labels_)
    obj_result = my_metrics.all(y_true, y_pred)
    return obj_result


class Executor:
    """
    Executor is a program that performs experiment based on the chosen algorithms.
    Here we determine -1 as inlier and 1 as outlier. That means, if an object
    is classified as outlier => positive, and inlier => negative.
    Example, if we use ABOD(14) and dataset 04vowels, it will return:
          TN | FP = 1308 | 98
          FN | TP =  2   | 48
    """

    @staticmethod
    def run_experiment(
        dataset_file_path,
        optimize_function,
        param_space_function,
        detector_name,
        reducer_name,
        max_eval,
    ):
        print(f"Extracting from {dataset_file_path}")
        dataset = loadmat(dataset_file_path)

        print(f"Running OD:{detector_name} and Reducer:{reducer_name}...")
        X_data, dataset_name, y_true = Executor._prepare_dataset(dataset)

        print("Preparing hyperparameter spaces..")
        param_space = param_space_function(X_data, y_true)

        print("Finding optimal hyperparameter..")
        hyperparameter = Executor._find_optimal_hyperparameter(
            detector_name, max_eval, optimize_function, param_space, reducer_name,
        )

        print("Finding result")
        print(f"{detector_name} {reducer_name}: {hyperparameter}")
        result, duration = Executor._find_optimal_result(
            hyperparameter, X_data, y_true, detector_name, reducer_name
        )

        print("Building report to Excel...")
        LocalReporter.build_report(
            dataset_name=dataset_name,
            hyperparameter=hyperparameter,
            result=result,
            duration=duration,
            detector_name=detector_name,
            reducer_name=reducer_name,
        )

    @staticmethod
    def _prepare_dataset(dataset):
        dataset_name = str(dataset["name"][0])
        print(f"Using {dataset_name}..")
        X_data = dataset["X"]
        y_true = prepare_y_true(dataset["y"])
        print(f"X_data and y_true is prepared. X_shape = {X_data.shape}.")
        return X_data, dataset_name, y_true

    @staticmethod
    def _find_optimal_hyperparameter(
        detector_name, max_eval, optimize_function, param_space, reducer_name,
    ):
        if detector_name == "COPOD" and reducer_name == "None":
            # It has to be a list since it will iterate at some point.
            hyperparameter = []
        else:
            experiment = fmin(
                optimize_function, param_space, algo=tpe.suggest, max_evals=max_eval
            )

            hyperparameter = Executor._prepare_optimal_hyperparameter(
                experiment, param_space
            )

        return hyperparameter

    @staticmethod
    def _find_optimal_result(
        hyperparameter, X_data, y_true, detector_name, reducer_name
    ):
        result = Executor.execute_optimal_result(
            hyperparameter, X_data, y_true, detector_name, reducer_name
        )

        return result

    @staticmethod
    def _prepare_optimal_hyperparameter(experiment, param_space):
        hyperparameter = []
        for i in range(len(param_space) - 2):
            hyperparameter.append(space_eval(param_space[i], experiment))
        # we need tuple because the next function will read each element automatically
        hyperparameter = tuple(hyperparameter)
        return hyperparameter

    @staticmethod
    def set_reducer_and_prune_hyperparameter(hyperparameter, reducer_name,feature_size):
        if "None" in reducer_name:
            return None, hyperparameter
        elif "PCA" in reducer_name:
            n_components = PCAOptimizer.get_n_components(hyperparameter[0],feature_size)
            hyperparameter = tuple(
                [hyperparameter[i] for i in range(1, len(hyperparameter))]
            )
            reducer = PCA(
                n_components,
                copy=True,
                whiten=False,
                svd_solver="auto",
                tol=0.0,
                iterated_power="auto",
                random_state=None,
            )
            return reducer, hyperparameter
        elif "UMAP" in reducer_name:
            hyperparameter = list(hyperparameter)
            n_neighbors = hyperparameter[0]
            n_components = UMAPOptimizer.get_n_components(hyperparameter[1], feature_size)
            n_epochs = hyperparameter[2]
            min_dist = hyperparameter[3]
            hyperparameter = tuple(
                [hyperparameter[i] for i in range(4, len(hyperparameter))]
            )
            reducer = UMAP(
                n_neighbors=n_neighbors,
                n_components=n_components,
                metric="euclidean",
                metric_kwds=None,
                output_metric="euclidean",
                output_metric_kwds=None,
                n_epochs=n_epochs,
                learning_rate=1.0,
                init="spectral",
                min_dist=min_dist,
                spread=1.0,
                low_memory=True,
                n_jobs=-1,
                set_op_mix_ratio=1.0,
                local_connectivity=1.0,
                repulsion_strength=1.0,
                negative_sample_rate=5,
                transform_queue_size=4.0,
                a=None,
                b=None,
                random_state=45,
                angular_rp_forest=False,
                target_n_neighbors=-1,
                target_metric="categorical",
                target_metric_kwds=None,
                target_weight=0.5,
                transform_seed=42,
                transform_mode="embedding",
                force_approximation_algorithm=False,
                verbose=True,
                unique=False,
                densmap=False,
                dens_lambda=2.0,
                dens_frac=0.3,
                dens_var_shift=0.1,
                output_dens=False,
                disconnection_distance=None,
            )
            return reducer,hyperparameter
        elif "tSNE" in reducer_name:
            hyperparameter = list(hyperparameter)
            n_components = TSNEOptimizer.get_n_components(hyperparameter[0],feature_size)
            perplexity = hyperparameter[1]
            learning_rate = hyperparameter[2]
            n_iter = hyperparameter[3]
            initial_momentum = hyperparameter[4]
            final_momentum = hyperparameter[5]
            hyperparameter = tuple(
                [hyperparameter[i] for i in range(6, len(hyperparameter))]
            )
            reducer = TSNE(
                n_components=n_components,
                perplexity=perplexity,
                learning_rate=learning_rate,
                early_exaggeration_iter=250,
                early_exaggeration=12,
                n_iter=n_iter,
                exaggeration=None,
                dof=1,
                theta=0.5,
                n_interpolation_points=3,
                min_num_intervals=50,
                ints_in_interval=1,
                initialization="random",
                metric="euclidean",
                metric_params=None,
                initial_momentum=initial_momentum,
                final_momentum=final_momentum,
                max_grad_norm=None,
                max_step_norm=5,
                n_jobs=-1,
                neighbors="auto",
                negative_gradient_method="auto",
                callbacks=None,
                callbacks_every_iters=50,
                random_state=45,
                verbose=True,
            )
            return reducer, hyperparameter
        elif "Autoencoder" in reducer_name:  # TODO: reduce with Autoencoder
            pass

    @staticmethod
    def set_detector(hyperparameter, detector_name):
        if "ABOD" in detector_name:
            n_neighbors = hyperparameter[0]
            return ABOD(contamination=0.1, n_neighbors=n_neighbors, method="fast")
        elif "COF" in detector_name:
            n_neighbors = int(hyperparameter[0])  # COF only accepts int not int64!
            return COF(contamination=0.1, n_neighbors=n_neighbors)
        elif "COPOD" in detector_name:
            return COPOD()
        elif "IForest" in detector_name:
            n_estimators, max_samples = hyperparameter
            return IForest(
                n_estimators=n_estimators,
                max_samples=max_samples,
                contamination=0.1,
                max_features=1.0,
                bootstrap=False,
                n_jobs=1,
                behaviour="old",
                random_state=45,
                verbose=0,
            )
        elif "iNNE" in detector_name:
            n_members, sample_size = hyperparameter
            return iNNE(
                n_members=n_members,
                sample_size=sample_size,
                contamination=0.1,
                metric="euclidean",
                tol=1e-8,
                verbose=False,
            )
        elif "KNN" in detector_name:
            n_neighbors, method = hyperparameter
            return KNN(
                contamination=0.1,
                n_neighbors=n_neighbors,
                method=method,
                radius=1.0,
                algorithm="auto",
                leaf_size=30,
                metric="minkowski",
                p=2,
                metric_params=None,
                n_jobs=-1,
            )

        elif "LOF" in detector_name:
            n_neighbors = hyperparameter[0]
            return LOF(
                n_neighbors=n_neighbors,
                algorithm="auto",
                leaf_size=30,
                metric="minkowski",
                p=2,
                metric_params=None,
                contamination=0.1,
                n_jobs=-1,
            )

    @staticmethod
    def execute_optimal_result(
        hyperparameter, X_data, y_true, detector_name, reducer_name
    ):
        feature_size = X_data.shape[1]
        reducer, hyperparameter = Executor.set_reducer_and_prune_hyperparameter(
            hyperparameter, reducer_name,feature_size
        )

        duration = timedelta(
            days=0,
            seconds=0,
            microseconds=0,
            milliseconds=0,
            minutes=0,
            hours=0,
            weeks=0,
        )
        if reducer is not None:
            try:
                start = datetime.now()
                X_data = reducer.fit_transform(X_data)
                end = datetime.now()
            except AttributeError:
                start = datetime.now()
                X_data = reducer.fit(X_data)
                end = datetime.now()
            duration = end - start

        detector = Executor.set_detector(hyperparameter, detector_name)

        start = datetime.now()
        detector.fit(X_data)
        end = datetime.now()
        duration = duration + end - start

        y_pred = convert_y_to_sckit(detector.labels_)
        print(f"outlier={np.count_nonzero(y_pred == 1)}")
        print(f"inlier={np.count_nonzero(y_pred == -1)}")
        print(
            f"Confusion Matrix:\ntn,fp,fn,tp\n{confusion_matrix(y_true,y_pred).ravel()}"
        )
        result = my_metrics.calculate_all(y_true, y_pred)
        return result, duration


class PlainOptimizer:
    @staticmethod
    def optimize_abod(args):
        n_neighbors, X_data, y_true = args
        print(f"n_neighbors = {n_neighbors}")

        detector = ABOD(contamination=0.1, n_neighbors=n_neighbors, method="fast")
        obj_result = fit_and_calculate_obj_result(detector, X_data, y_true)

        return -obj_result

    @staticmethod
    def create_abod_spaces(X_data, y_true):
        return determine_n_neighbors_space(X_data.shape[0]), X_data, y_true

    @staticmethod
    def optimize_cof(args):
        n_neighbors, X_data, y_true = args
        n_neighbors = int(n_neighbors)  # COF can only accept int not int64!
        print(f"n_neighbors = {n_neighbors}")

        detector = COF(contamination=0.1, n_neighbors=n_neighbors)
        obj_result = fit_and_calculate_obj_result(detector, X_data, y_true)

        return -obj_result

    @staticmethod
    def create_cof_spaces(X_data, y_true):
        return determine_n_neighbors_space(X_data.shape[0]), X_data, y_true

    @staticmethod
    def run_copod(args):
        X_data, y_true = args

        # nothing to optimize
        detector = COPOD()
        obj_result = fit_and_calculate_obj_result(detector, X_data, y_true)

        return (
            obj_result  # it doesn't have to be negative, since COPOD is parameter free
        )

    # We need this function for generalizing the Executor
    @staticmethod
    def create_copod_spaces(X_data, y_true):
        return X_data, y_true

    @staticmethod
    def optimize_iforest(args):
        n_estimators, max_samples, X_data, y_true = args
        print(f"n_estimators = {n_estimators}")
        print(f"max_samples = {max_samples}")

        detector = IForest(
            n_estimators=n_estimators,
            max_samples=max_samples,
            contamination=0.1,
            max_features=1.0,
            bootstrap=False,
            n_jobs=-1,
            behaviour="old",
            random_state=45,
            verbose=0,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data, y_true)

        return -obj_result

    @staticmethod
    def create_iforest_spaces(X_data, y_true):
        return (
            N_ESTIMATORS_SPACE,
            determine_sample_size_space(X_data.shape[0]),
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_inne(args):
        n_members, sample_size, X_data, y_true = args
        print(f"n_members = {n_members}")
        print(f"sample_size = {sample_size}")

        detector = iNNE(
            n_members=n_members,
            sample_size=sample_size,
            contamination=0.1,
            metric="euclidean",
            tol=1e-8,
            verbose=False,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data, y_true)

        return -obj_result

    @staticmethod
    def create_inne_spaces(X_data, y_true):
        return (
            N_ESTIMATORS_SPACE,
            determine_sample_size_space(X_data.shape[0]),
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_knn(args):
        n_neighbors, method, X_data, y_true = args
        print(f"n_neighbors = {n_neighbors}")
        print(f"method = {method}")

        detector = KNN(
            contamination=0.1,
            n_neighbors=n_neighbors,
            method=method,
            radius=1.0,
            algorithm="auto",
            leaf_size=30,
            metric="minkowski",
            p=2,
            metric_params=None,
            n_jobs=-1,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data, y_true)

        return -obj_result

    @staticmethod
    def create_knn_spaces(X_data, y_true):
        return (
            determine_n_neighbors_space(X_data.shape[0]),
            KNN_METHOD_SPACE,
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_lof(args):
        n_neighbors, X_data, y_true = args
        print(f"n_neighbors = {n_neighbors}")

        detector = LOF(
            n_neighbors=n_neighbors,
            algorithm="auto",
            leaf_size=30,
            metric="minkowski",
            p=2,
            metric_params=None,
            contamination=0.1,
            n_jobs=-1,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data, y_true)

        return -obj_result

    @staticmethod
    def create_lof_spaces(X_data, y_true):
        return determine_n_neighbors_space(X_data.shape[0]), X_data, y_true


class PCAOptimizer:
    @staticmethod
    def get_n_components(n_components_percentage, feature_size):
        n_components_percentage = ceil(n_components_percentage * feature_size)
        if n_components_percentage == feature_size:
            n_components_percentage -= 1
        return n_components_percentage

    @staticmethod
    def reduce_dimension(n_components, X_data):
        my_PCA = PCA(
            n_components,
            copy=True,
            whiten=False,
            svd_solver="auto",
            tol=0.0,
            iterated_power="auto",
            random_state=None,
        )
        X_data_reduced = my_PCA.fit_transform(X_data)

        return X_data_reduced

    @staticmethod
    def optimize_abod(args):
        n_components_percentage, n_neighbors, X_data, y_true = args
        print(f"n_neighbors = {n_neighbors}")

        n_components = PCAOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        print(f"n_components = {n_components}")
        X_data_reduced = PCAOptimizer.reduce_dimension(n_components, X_data)

        detector = ABOD(contamination=0.1, n_neighbors=n_neighbors, method="fast")
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_abod_spaces(X_data, y_true):
        return (
            N_COMPONENTS_SPACE,
            determine_n_neighbors_space(X_data.shape[0]),
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_cof(args):
        n_components_percentage, n_neighbors, X_data, y_true = args
        n_neighbors = int(n_neighbors)  # n_neighbors should be int not int64!
        print(f"n_neighbors = {n_neighbors}")

        n_components = PCAOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        print(f"n_components = {n_components}")
        X_data_reduced = PCAOptimizer.reduce_dimension(n_components, X_data)

        detector = COF(contamination=0.1, n_neighbors=n_neighbors)
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_cof_spaces(X_data, y_true):
        return (
            N_COMPONENTS_SPACE,
            determine_n_neighbors_space(X_data.shape[0]),
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_copod(args):
        n_components_percentage, X_data, y_true = args

        n_components = PCAOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        print(f"n_components = {n_components}")
        X_data_reduced = PCAOptimizer.reduce_dimension(n_components, X_data)

        # nothing to optimize
        detector = COPOD()
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_copod_spaces(X_data, y_true):
        return N_COMPONENTS_SPACE, X_data, y_true

    @staticmethod
    def optimize_iforest(args):
        n_components_percentage, n_estimators, max_samples, X_data, y_true = args
        print(f"n_estimators = {n_estimators}")
        print(f"max_samples = {max_samples}")

        n_components = PCAOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        print(f"n_components = {n_components}")
        X_data_reduced = PCAOptimizer.reduce_dimension(n_components, X_data)

        detector = IForest(
            n_estimators=n_estimators,
            max_samples=max_samples,
            contamination=0.1,
            max_features=1.0,
            bootstrap=False,
            n_jobs=-1,
            behaviour="old",
            random_state=45,
            verbose=0,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_iforest_spaces(X_data, y_true):
        return (
            N_COMPONENTS_SPACE,
            N_ESTIMATORS_SPACE,
            determine_sample_size_space(X_data.shape[0]),
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_inne(args):
        n_components_percentage, n_members, sample_size, X_data, y_true = args
        print(f"n_members = {n_members}")
        print(f"sample_size = {sample_size}")

        n_components = PCAOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        print(f"n_components = {n_components}")
        X_data_reduced = PCAOptimizer.reduce_dimension(n_components, X_data)

        detector = iNNE(
            n_members=n_members,
            sample_size=sample_size,
            contamination=0.1,
            metric="euclidean",
            tol=1e-8,
            verbose=False,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_inne_spaces(X_data, y_true):
        return (
            N_COMPONENTS_SPACE,
            N_ESTIMATORS_SPACE,
            determine_sample_size_space(X_data.shape[0]),
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_knn(args):
        n_components_percentage, n_neighbors, method, X_data, y_true = args
        print(f"n_neighbors = {n_neighbors}")
        print(f"method = {method}")

        n_components = PCAOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        print(f"n_components = {n_components}")
        X_data_reduced = PCAOptimizer.reduce_dimension(n_components, X_data)

        detector = KNN(
            contamination=0.1,
            n_neighbors=n_neighbors,
            method=method,
            radius=1.0,
            algorithm="auto",
            leaf_size=30,
            metric="minkowski",
            p=2,
            metric_params=None,
            n_jobs=-1,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_knn_spaces(X_data, y_true):
        return (
            N_COMPONENTS_SPACE,
            determine_n_neighbors_space(X_data.shape[0]),
            KNN_METHOD_SPACE,
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_lof(args):
        n_components_percentage, n_neighbors, X_data, y_true = args
        print(f"n_neighbors = {n_neighbors}")

        n_components = PCAOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        print(f"n_components = {n_components}")
        X_data_reduced = PCAOptimizer.reduce_dimension(n_components, X_data)

        detector = LOF(
            n_neighbors=n_neighbors,
            algorithm="auto",
            leaf_size=30,
            metric="minkowski",
            p=2,
            metric_params=None,
            contamination=0.1,
            n_jobs=-1,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_lof_spaces(X_data, y_true):
        return (
            N_COMPONENTS_SPACE,
            determine_n_neighbors_space(X_data.shape[0]),
            X_data,
            y_true,
        )


class TSNEOptimizer:
    @staticmethod
    def get_n_components(n_components_percentage, feature_size):
        n_components_percentage = ceil(n_components_percentage * feature_size)
        if n_components_percentage == feature_size:
            n_components_percentage -= 1
        return n_components_percentage

    @staticmethod
    def reduce_dimension(
        n_components,
        perplexity,
        learning_rate,
        n_iter,
        initial_momentum,
        final_momentum,
        X_data,
    ):
        my_TSNE = TSNE(
            n_components=n_components,
            perplexity=perplexity,
            learning_rate=learning_rate,
            early_exaggeration_iter=250,
            early_exaggeration=12,
            n_iter=n_iter,
            exaggeration=None,
            dof=1,
            theta=0.5,
            n_interpolation_points=3,
            min_num_intervals=50,
            ints_in_interval=1,
            initialization="random",
            metric="euclidean",
            metric_params=None,
            initial_momentum=initial_momentum,
            final_momentum=final_momentum,
            max_grad_norm=None,
            max_step_norm=5,
            n_jobs=-1,
            neighbors="auto",
            negative_gradient_method="auto",
            callbacks=None,
            callbacks_every_iters=50,
            random_state=45,
            verbose=True,
        )
        X_data_reduced = my_TSNE.fit(X_data)

        return X_data_reduced

    @staticmethod
    def optimize_abod(args):
        (
            n_components_percentage,
            perplexity,
            learning_rate,
            n_iter,
            initial_momentum,
            final_momentum,
            n_neighbors,
            X_data,
            y_true,
        ) = args
        print(f"n_neighbors = {n_neighbors}")

        n_components = TSNEOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        X_data_reduced = TSNEOptimizer.reduce_dimension(
            n_components,
            perplexity,
            learning_rate,
            n_iter,
            initial_momentum,
            final_momentum,
            X_data,
        )

        detector = ABOD(contamination=0.1, n_neighbors=n_neighbors, method="fast")
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_abod_spaces(X_data, y_true):
        return (
            N_COMPONENTS_SPACE,
            PERPLEXITY_SPACE,
            TSNE_LEARNING_RATE_SPACE,
            TSNE_N_ITER_SPACE,
            TSNE_INIT_MOMENTUM_SPACE,
            TSNE_FINAL_MOMENTUM_SPACE,
            determine_n_neighbors_space(X_data.shape[0]),
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_cof(args):
        (
            n_components_percentage,
            perplexity,
            learning_rate,
            n_iter,
            initial_momentum,
            final_momentum,
            n_neighbors,
            X_data,
            y_true,
        ) = args
        n_neighbors = int(n_neighbors)  # n_neighbors should be int not int64!
        print(f"n_neighbors = {n_neighbors}")

        n_components = TSNEOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        X_data_reduced = TSNEOptimizer.reduce_dimension(
            n_components,
            perplexity,
            learning_rate,
            n_iter,
            initial_momentum,
            final_momentum,
            X_data,
        )

        detector = COF(contamination=0.1, n_neighbors=n_neighbors)
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_cof_spaces(X_data, y_true):
        return (
            N_COMPONENTS_SPACE,
            PERPLEXITY_SPACE,
            TSNE_LEARNING_RATE_SPACE,
            TSNE_N_ITER_SPACE,
            TSNE_INIT_MOMENTUM_SPACE,
            TSNE_FINAL_MOMENTUM_SPACE,
            determine_n_neighbors_space(X_data.shape[0]),
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_copod(args):
        (
            n_components_percentage,
            perplexity,
            learning_rate,
            n_iter,
            initial_momentum,
            final_momentum,
            X_data,
            y_true,
        ) = args

        n_components = TSNEOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        X_data_reduced = TSNEOptimizer.reduce_dimension(
            n_components,
            perplexity,
            learning_rate,
            n_iter,
            initial_momentum,
            final_momentum,
            X_data,
        )

        # nothing to optimize
        detector = COPOD()
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_copod_spaces(X_data, y_true):
        return (
            N_COMPONENTS_SPACE,
            PERPLEXITY_SPACE,
            TSNE_LEARNING_RATE_SPACE,
            TSNE_N_ITER_SPACE,
            TSNE_INIT_MOMENTUM_SPACE,
            TSNE_FINAL_MOMENTUM_SPACE,
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_iforest(args):
        (
            n_components_percentage,
            perplexity,
            learning_rate,
            n_iter,
            initial_momentum,
            final_momentum,
            n_estimators,
            max_samples,
            X_data,
            y_true,
        ) = args
        print(f"n_estimators = {n_estimators}")
        print(f"max_samples = {max_samples}")

        n_components = TSNEOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )

        X_data_reduced = TSNEOptimizer.reduce_dimension(
            n_components,
            perplexity,
            learning_rate,
            n_iter,
            initial_momentum,
            final_momentum,
            X_data,
        )

        detector = IForest(
            n_estimators=n_estimators,
            max_samples=max_samples,
            contamination=0.1,
            max_features=1.0,
            bootstrap=False,
            n_jobs=1,
            behaviour="old",
            random_state=45,
            verbose=0,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_iforest_spaces(X_data, y_true):
        return (
            N_COMPONENTS_SPACE,
            PERPLEXITY_SPACE,
            TSNE_LEARNING_RATE_SPACE,
            TSNE_N_ITER_SPACE,
            TSNE_INIT_MOMENTUM_SPACE,
            TSNE_FINAL_MOMENTUM_SPACE,
            N_ESTIMATORS_SPACE,
            determine_sample_size_space(X_data.shape[0]),
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_inne(args):
        (
            n_components_percentage,
            perplexity,
            learning_rate,
            n_iter,
            initial_momentum,
            final_momentum,
            n_members,
            sample_size,
            X_data,
            y_true,
        ) = args
        print(f"n_members = {n_members}")
        print(f"sample_size = {sample_size}")

        n_components = TSNEOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        X_data_reduced = TSNEOptimizer.reduce_dimension(
            n_components,
            perplexity,
            learning_rate,
            n_iter,
            initial_momentum,
            final_momentum,
            X_data,
        )

        detector = iNNE(
            n_members=n_members,
            sample_size=sample_size,
            contamination=0.1,
            metric="euclidean",
            tol=1e-8,
            verbose=False,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_inne_spaces(X_data, y_true):
        return (
            N_COMPONENTS_SPACE,
            PERPLEXITY_SPACE,
            TSNE_LEARNING_RATE_SPACE,
            TSNE_N_ITER_SPACE,
            TSNE_INIT_MOMENTUM_SPACE,
            TSNE_FINAL_MOMENTUM_SPACE,
            N_ESTIMATORS_SPACE,
            determine_sample_size_space(X_data.shape[0]),
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_knn(args):
        (
            n_components_percentage,
            perplexity,
            learning_rate,
            n_iter,
            initial_momentum,
            final_momentum,
            n_neighbors,
            method,
            X_data,
            y_true,
        ) = args
        print(f"n_neighbors = {n_neighbors}")
        print(f"method = {method}")

        n_components = TSNEOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        X_data_reduced = TSNEOptimizer.reduce_dimension(
            n_components,
            perplexity,
            learning_rate,
            n_iter,
            initial_momentum,
            final_momentum,
            X_data,
        )

        detector = KNN(
            contamination=0.1,
            n_neighbors=n_neighbors,
            method=method,
            radius=1.0,
            algorithm="auto",
            leaf_size=30,
            metric="minkowski",
            p=2,
            metric_params=None,
            n_jobs=-1,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_knn_spaces(X_data, y_true):
        return (
            N_COMPONENTS_SPACE,
            PERPLEXITY_SPACE,
            TSNE_LEARNING_RATE_SPACE,
            TSNE_N_ITER_SPACE,
            TSNE_INIT_MOMENTUM_SPACE,
            TSNE_FINAL_MOMENTUM_SPACE,
            determine_n_neighbors_space(X_data.shape[0]),
            KNN_METHOD_SPACE,
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_lof(args):
        (
            n_components_percentage,
            perplexity,
            learning_rate,
            n_iter,
            initial_momentum,
            final_momentum,
            n_neighbors,
            X_data,
            y_true,
        ) = args
        print(f"n_neighbors = {n_neighbors}")

        n_components = TSNEOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        X_data_reduced = TSNEOptimizer.reduce_dimension(
            n_components,
            perplexity,
            learning_rate,
            n_iter,
            initial_momentum,
            final_momentum,
            X_data,
        )

        detector = LOF(
            n_neighbors=n_neighbors,
            algorithm="auto",
            leaf_size=30,
            metric="minkowski",
            p=2,
            metric_params=None,
            contamination=0.1,
            n_jobs=-1,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_lof_spaces(X_data, y_true):
        return (
            N_COMPONENTS_SPACE,
            PERPLEXITY_SPACE,
            TSNE_LEARNING_RATE_SPACE,
            TSNE_N_ITER_SPACE,
            TSNE_INIT_MOMENTUM_SPACE,
            TSNE_FINAL_MOMENTUM_SPACE,
            determine_n_neighbors_space(X_data.shape[0]),
            X_data,
            y_true,
        )


class UMAPOptimizer:
    @staticmethod
    def get_n_components(n_components_percentage, feature_size):
        n_components_percentage = ceil(n_components_percentage * feature_size)
        if n_components_percentage == feature_size:
            n_components_percentage -= 1
        return n_components_percentage

    @staticmethod
    def reduce_dimension(
        n_neighbors, n_components, n_epochs, min_dist, X_data,
    ):
        my_UMAP = UMAP(
            n_neighbors=n_neighbors,
            n_components=n_components,
            metric="euclidean",
            metric_kwds=None,
            output_metric="euclidean",
            output_metric_kwds=None,
            n_epochs=n_epochs,
            learning_rate=1.0,
            init="spectral",
            min_dist=min_dist,
            spread=1.0,
            low_memory=True,
            n_jobs=-1,
            set_op_mix_ratio=1.0,
            local_connectivity=1.0,
            repulsion_strength=1.0,
            negative_sample_rate=5,
            transform_queue_size=4.0,
            a=None,
            b=None,
            random_state=45,
            angular_rp_forest=False,
            target_n_neighbors=-1,
            target_metric="categorical",
            target_metric_kwds=None,
            target_weight=0.5,
            transform_seed=42,
            transform_mode="embedding",
            force_approximation_algorithm=False,
            verbose=True,
            unique=False,
            densmap=False,
            dens_lambda=2.0,
            dens_frac=0.3,
            dens_var_shift=0.1,
            output_dens=False,
            disconnection_distance=None,
        )

        X_data_reduced = my_UMAP.fit_transform(X_data)

        return X_data_reduced

    @staticmethod
    def optimize_abod(args):
        (
            umap_n_neighbors,
            n_components_percentage,
            n_epochs,
            min_dist,
            n_neighbors,
            X_data,
            y_true,
        ) = args
        print(f"n_neighbors = {n_neighbors}")

        n_components = UMAPOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        X_data_reduced = UMAPOptimizer.reduce_dimension(
            umap_n_neighbors, n_components, n_epochs, min_dist, X_data,
        )

        detector = ABOD(contamination=0.1, n_neighbors=n_neighbors, method="fast")
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_abod_spaces(X_data, y_true):
        return (
            determine_umap_n_neighbors_space(X_data.shape[0]),
            N_COMPONENTS_SPACE,
            UMAP_EPOCH,
            UMAP_MIN_DIST,
            determine_n_neighbors_space(X_data.shape[0]),
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_cof(args):
        (
            umap_n_neighbors,
            n_components_percentage,
            n_epochs,
            min_dist,
            n_neighbors,
            X_data,
            y_true,
        ) = args
        n_neighbors = int(n_neighbors)  # n_neighbors should be int not int64!
        print(f"n_neighbors = {n_neighbors}")

        n_components = UMAPOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        X_data_reduced = UMAPOptimizer.reduce_dimension(
            umap_n_neighbors, n_components, n_epochs, min_dist, X_data,
        )

        detector = COF(contamination=0.1, n_neighbors=n_neighbors)
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_cof_spaces(X_data, y_true):
        return (
            determine_umap_n_neighbors_space(X_data.shape[0]),
            N_COMPONENTS_SPACE,
            UMAP_EPOCH,
            UMAP_MIN_DIST,
            determine_n_neighbors_space(X_data.shape[0]),
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_copod(args):
        (
            umap_n_neighbors,
            n_components_percentage,
            n_epochs,
            min_dist,
            X_data,
            y_true,
        ) = args

        n_components = UMAPOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        X_data_reduced = UMAPOptimizer.reduce_dimension(
            umap_n_neighbors, n_components, n_epochs, min_dist, X_data,
        )

        # nothing to optimize
        detector = COPOD()
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_copod_spaces(X_data, y_true):
        return (
            determine_umap_n_neighbors_space(X_data.shape[0]),
            N_COMPONENTS_SPACE,
            UMAP_EPOCH,
            UMAP_MIN_DIST,
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_iforest(args):
        (
            umap_n_neighbors,
            n_components_percentage,
            n_epochs,
            min_dist,
            n_estimators,
            max_samples,
            X_data,
            y_true,
        ) = args
        print(f"n_estimators = {n_estimators}")
        print(f"max_samples = {max_samples}")

        n_components = UMAPOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )

        X_data_reduced = UMAPOptimizer.reduce_dimension(
            umap_n_neighbors, n_components, n_epochs, min_dist, X_data,
        )

        detector = IForest(
            n_estimators=n_estimators,
            max_samples=max_samples,
            contamination=0.1,
            max_features=1.0,
            bootstrap=False,
            n_jobs=1,
            behaviour="old",
            random_state=45,
            verbose=0,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_iforest_spaces(X_data, y_true):
        return (
            determine_umap_n_neighbors_space(X_data.shape[0]),
            N_COMPONENTS_SPACE,
            UMAP_EPOCH,
            UMAP_MIN_DIST,
            N_ESTIMATORS_SPACE,
            determine_sample_size_space(X_data.shape[0]),
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_inne(args):
        (
            umap_n_neighbors,
            n_components_percentage,
            n_epochs,
            min_dist,
            n_members,
            sample_size,
            X_data,
            y_true,
        ) = args
        print(f"n_members = {n_members}")
        print(f"sample_size = {sample_size}")

        n_components = UMAPOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        X_data_reduced = UMAPOptimizer.reduce_dimension(
            umap_n_neighbors, n_components, n_epochs, min_dist, X_data,
        )

        detector = iNNE(
            n_members=n_members,
            sample_size=sample_size,
            contamination=0.1,
            metric="euclidean",
            tol=1e-8,
            verbose=False,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_inne_spaces(X_data, y_true):
        return (
            determine_umap_n_neighbors_space(X_data.shape[0]),
            N_COMPONENTS_SPACE,
            UMAP_EPOCH,
            UMAP_MIN_DIST,
            N_ESTIMATORS_SPACE,
            determine_sample_size_space(X_data.shape[0]),
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_knn(args):
        (
            umap_n_neighbors,
            n_components_percentage,
            n_epochs,
            min_dist,
            n_neighbors,
            method,
            X_data,
            y_true,
        ) = args
        print(f"n_neighbors = {n_neighbors}")
        print(f"method = {method}")

        n_components = UMAPOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        X_data_reduced = UMAPOptimizer.reduce_dimension(
            umap_n_neighbors, n_components, n_epochs, min_dist, X_data,
        )

        detector = KNN(
            contamination=0.1,
            n_neighbors=n_neighbors,
            method=method,
            radius=1.0,
            algorithm="auto",
            leaf_size=30,
            metric="minkowski",
            p=2,
            metric_params=None,
            n_jobs=-1,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_knn_spaces(X_data, y_true):
        return (
            determine_umap_n_neighbors_space(X_data.shape[0]),
            N_COMPONENTS_SPACE,
            UMAP_EPOCH,
            UMAP_MIN_DIST,
            determine_n_neighbors_space(X_data.shape[0]),
            KNN_METHOD_SPACE,
            X_data,
            y_true,
        )

    @staticmethod
    def optimize_lof(args):
        (
            umap_n_neighbors,
            n_components_percentage,
            n_epochs,
            min_dist,
            n_neighbors,
            X_data,
            y_true,
        ) = args
        print(f"n_neighbors = {n_neighbors}")

        n_components = UMAPOptimizer.get_n_components(
            n_components_percentage, X_data.shape[1]
        )
        X_data_reduced = UMAPOptimizer.reduce_dimension(
            umap_n_neighbors, n_components, n_epochs, min_dist, X_data,
        )

        detector = LOF(
            n_neighbors=n_neighbors,
            algorithm="auto",
            leaf_size=30,
            metric="minkowski",
            p=2,
            metric_params=None,
            contamination=0.1,
            n_jobs=-1,
        )
        obj_result = fit_and_calculate_obj_result(detector, X_data_reduced, y_true)

        return -obj_result

    @staticmethod
    def create_lof_spaces(X_data, y_true):
        return (
            determine_umap_n_neighbors_space(X_data.shape[0]),
            N_COMPONENTS_SPACE,
            UMAP_EPOCH,
            UMAP_MIN_DIST,
            determine_n_neighbors_space(X_data.shape[0]),
            X_data,
            y_true,
        )


class AutoencoderOptimizer:
    pass
