from hyperopt import hp

N_NEIGHBORS_SPACE = hp.randint("n_neighbors", 5, 200)
N_ESTIMATORS_SPACE = hp.randint("n_estimators", 5, 200)
KNN_METHOD_SPACE = hp.choice("knn_method", ["largest", "mean", "median"])
N_COMPONENTS_SPACE = hp.choice("n_components_space", [0.1, 0.2, 0.4, 0.6, 0.8, 0.9])
PERPLEXITY_SPACE = hp.randint("perplexity", 5, 50)
TSNE_LEARNING_RATE_SPACE = hp.uniform("learning_rate", 10, 1000)
TSNE_N_ITER_SPACE = hp.randint("n_iteration", 250, 1500)
TSNE_INIT_MOMENTUM_SPACE = hp.uniform("init_momentum", 0.3, 0.7)
TSNE_FINAL_MOMENTUM_SPACE = hp.uniform("final_momentum", 0.7, 0.99)
UMAP_EPOCH = hp.randint("epochs", 100, 600)
UMAP_MIN_DIST = hp.uniform("min_dist", 0, 1)


def determine_n_neighbors_space(data_size):
    if data_size <= 200:
        return hp.randint("n_neighbors", 5, int(0.7 * data_size))
    else:
        return hp.randint("n_neighbors", 5, 200)

def determine_umap_n_neighbors_space(data_size):
    if data_size <= 200:
        return hp.randint("umap_n_neighbors", 5, int(0.7 * data_size))
    else:
        return hp.randint("umap_n_neighbors", 5, 200)


def determine_sample_size_space(data_size):
    if data_size <= 200:
        return hp.randint("sample_size", 5, int(0.8 * data_size))
    else:
        return hp.randint("sample_size", 5, 200)
