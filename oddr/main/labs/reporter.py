from datetime import datetime
from oddr.main.pysheet.pysheet_client import PysheetClient


class LocalReporter:
    @staticmethod
    def insert_hyperparam_detector(detector, column):
        insert_position = column.index("obj_result")
        if "ABOD" in detector or "COF" in detector or "LOF" in detector:
            column.insert(insert_position, "n_neighbors")
        elif "KNN" in detector:
            column.insert(insert_position, "n_neighbors")
            column.insert(insert_position + 1, "method")
        elif "IForest" in detector:
            column.insert(insert_position, "n_estimators")
            column.insert(insert_position + 1, "max_sample")
        elif "iNNE" in detector:
            column.insert(insert_position, "n_members")
            column.insert(insert_position + 1, "sample_size")

        return column

    @staticmethod
    def insert_hyperparam_reducer(column, reducer):
        insert_position = column.index("obj_result")
        if "PCA" in reducer:
            column.insert(insert_position, "n_components")
        elif "tSNE" in reducer:
            column.insert(insert_position, "n_components")
            column.insert(insert_position + 1, "perplexity")
            column.insert(insert_position + 2, "learning_rate")
            column.insert(insert_position + 3, "n_iter")
            column.insert(insert_position + 4, "initial_momentum")
            column.insert(insert_position + 5, "final_momentum")
        elif "UMAP" in reducer:
            column.insert(insert_position, "umap_n_neighbors")
            column.insert(insert_position + 1, "n_components")
            column.insert(insert_position + 2, "umap_epochs")
            column.insert(insert_position + 3, "umap_min_dist")

        return column

    @staticmethod
    def get_column_name(detector, reducer):
        column = [
            "datetime",
            "detector",
            "dataset_name",
            "obj_result",
            "f1_score",
            "accuracy_score",
            "precision_score",
            "recall_score",
            "matthew_corrcoef",
            "cohen_kappa_score",
            "duration",
        ]

        column = LocalReporter.insert_hyperparam_reducer(column, reducer)
        column = LocalReporter.insert_hyperparam_detector(detector, column)

        return column

    @staticmethod
    def build_value_list(hyperparameter, result, detector_name, dataset_name, duration):
        (
            obj_result,
            f1_score,
            accuracy_score,
            precision_score,
            recall_score,
            matthew_corrcoef,
            cohen_kappa_score,
        ) = result

        time = datetime.now()
        values = [
            time,
            detector_name,
            dataset_name,
            obj_result,
            f1_score,
            accuracy_score,
            precision_score,
            recall_score,
            matthew_corrcoef,
            cohen_kappa_score,
            duration,
        ]

        insert_position = 3
        for value in hyperparameter:
            values.insert(insert_position, value)
            insert_position += 1

        return values

    @staticmethod
    def build_report(
        dataset_name, hyperparameter, result, duration, detector_name, reducer_name
    ):
        pysheet_client = PysheetClient(f"{detector_name}_Report.xlsx")
        pysheet_client.access_spreadsheet(reducer_name)

        if not pysheet_client.column_name_exist():
            column = LocalReporter.get_column_name(detector_name, reducer_name)
            pysheet_client.create_column_name(column)

        values = LocalReporter.build_value_list(
            hyperparameter, result, detector_name, dataset_name, duration
        )

        pysheet_client.write_report(values)
        pysheet_client.save()
