import path_manager

from oddr.main.pysheet.pysheet_client import PysheetClient


def decode_row_cell(row_cell_list):
    value_list = []
    for cell in row_cell_list:
        value = cell.value
        value_list.append(value)
    return value_list


if __name__ == "__main__":
    pysheet_client = PysheetClient("ABOD_None_experiment.xlsx")
    worksheet_names = pysheet_client.get_worksheet_names()
    for i in range(1, len(worksheet_names)):
        pysheet_client.access_spreadsheet(worksheet_names[i])
        for row_index in range(pysheet_client.get_all_row_cell_size()):
            if i != 1 and row_index == 0:
                continue
            pysheet_client.access_spreadsheet(worksheet_names[i])
            row_cell_list = pysheet_client.get_row_cell(row_index)
            value_list = decode_row_cell(row_cell_list)
            if row_index == 0:
                value_list.append("dataset")
            else:
                value_list.append(worksheet_names[i])
            pysheet_client.access_spreadsheet("Sheet")
            pysheet_client.write_report(value_list)

    pysheet_client.save()
