from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from pydrive.auth import ServiceAccountCredentials


class MyGDrive:
    @staticmethod
    def _authenticate():
        gauth = GoogleAuth()
        scope = ["https://www.googleapis.com/auth/drive"]
        gauth.credentials = ServiceAccountCredentials.from_json_keyfile_name(
            "gCredentials.json", scope
        )
        drive = GoogleDrive(gauth)

        return drive

    @staticmethod
    def upload(filename,folder_id):
        drive = MyGDrive._authenticate()
        folder = drive.CreateFile(
            {
                "parents": [{"id": folder_id}],
                "title": filename,
                "mimeType": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            }
        )
        folder.SetContentFile(filename)
        folder.Upload()
