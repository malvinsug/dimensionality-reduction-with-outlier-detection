import numpy as np


def convert_y_to_sckit(y: np.ndarray):
    """
    convert y into sckit format, where 1 is outlier and -1 is inlier.
    We want the outlier to be the positive one.
    :param y: array like for y
    :return: y variable in sckit format
    """
    elements = np.unique(y)
    if -1 in elements:
        return y

    y[y == 0] = -1
    return y


def check_y_true_type(y_true: np.ndarray):
    if str(y_true.dtype) != "float64":
        y_true = y_true.astype(np.float64)
    return y_true


def prepare_y_true(y):
    y_true = check_y_true_type(y)
    y_true = convert_y_to_sckit(y_true)
    return y_true
