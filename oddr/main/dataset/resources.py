import os


class Resources:
    DATASET_DIR_PATH = os.getenv("DATA_DIR")
    DATASET_NAME_LIST = [
        "26vertebral.mat",
        "25wine.mat",
        "03glass.mat",
        "24ecoli.mat",
        "29SPECTF_train.mat",
        "11pima.mat",
        "14breastw.mat",
        "29SPECTF_test.mat",
        "27yeast.mat",
        "02wbc.mat",
        "16ionosphere.mat",
        "30cluto-t4-8k.mat",
        "30cluto-t5-8k.mat",
        "30cluto-t8-8k.mat",
        "04vowels.mat",
        "30cluto-t7-10k.mat",
        "06thyroid.mat",
        "05cardio.mat",
        "22annthyroid.mat",
        "09letter.mat",
        "21mammography.mat",
        "23pendigits.mat",
        "08satimage-2.mat",
        "12satellite.mat",
        "20smtp.mat",
        "32optdigits.mat",
        "13shuttle.mat",
        "07musk.mat",
        "17mnist.mat",
        "31mulcross.mat",
        "10speech.mat",
    ]


if __name__ == "__main__":
    from scipy.io import loadmat, savemat
    import os
    import numpy as np

    path = os.getenv("DATA_DIR")
    data = loadmat(f"{path}/13shuttle.mat")
    X = np.float32(data["X"])
    data["X"] = X
    savemat(f"{path}/13shuttle.mat", data)
