import openpyxl


class PysheetClient:
    def __init__(self, filename):
        try:
            workbook = openpyxl.load_workbook(filename)
        except FileNotFoundError:
            workbook = openpyxl.Workbook()

        self.workbook = workbook
        self.spreadsheet = None
        self.name = filename

    def access_spreadsheet(self, sheet_name):
        try:
            self.workbook.active = self.workbook[sheet_name]
            self.spreadsheet = self.workbook.get_active_sheet()
        except KeyError:
            self.workbook.create_sheet(sheet_name)
            self.workbook.active = self.workbook[sheet_name]
            self.spreadsheet = self.workbook.get_active_sheet()

    def create_column_name(self, column_list):
        for j in range(len(column_list)):
            cell = self.spreadsheet.cell(1, j + 1)
            cell.value = column_list[j]

    def column_name_exist(self):
        return self.spreadsheet.cell(1, 1).value is not None

    def find_empty_row(self):
        row_index = 1
        cell = self.spreadsheet.cell(row_index, 1)
        while cell.value is not None:
            row_index += 1
            cell = self.spreadsheet.cell(row_index, 1)

        return row_index

    def write_report(self, report_list):
        row_index = self.find_empty_row()
        for report_index in range(len(report_list)):
            cell = self.spreadsheet.cell(row_index, report_index + 1)
            cell.value = report_list[report_index]

    def save(self):
        self.workbook.save(self.name)

    def get_row_cell(self, row_number):
        return list(self.spreadsheet.rows)[row_number]

    def get_all_row_cell_size(self):
        return len(list(self.spreadsheet.rows))

    def get_worksheet_names(self):
        return self.workbook.sheetnames
