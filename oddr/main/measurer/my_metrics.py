from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import auc
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import cohen_kappa_score


def f1(
    y_true,
    y_pred,
    labels=None,
    pos_label=1,
    average="binary",
    sample_weight=None,
    zero_division="warn",
):
    f1 = f1_score(
        y_true,
        y_pred,
        labels=labels,
        pos_label=pos_label,
        average=average,
        sample_weight=sample_weight,
        zero_division=zero_division,
    )
    return f1


def accuracy(y_true, y_pred, normalize=True, sample_weight=None):
    accuracy = accuracy_score(
        y_true, y_pred, normalize=normalize, sample_weight=sample_weight
    )
    return accuracy


def precision(
    y_true,
    y_pred,
    labels=None,
    pos_label=1,
    average="binary",
    sample_weight=None,
    zero_division="warn",
):
    precision = precision_score(
        y_true,
        y_pred,
        labels=labels,
        pos_label=pos_label,
        average=average,
        sample_weight=sample_weight,
        zero_division=zero_division,
    )
    return precision


def recall(
    y_true,
    y_pred,
    labels=None,
    pos_label=1,
    average="binary",
    sample_weight=None,
    zero_division="warn",
):
    recall = recall_score(
        y_true,
        y_pred,
        labels=labels,
        pos_label=pos_label,
        average=average,
        sample_weight=sample_weight,
        zero_division=zero_division,
    )
    return recall


"""
def auc_score(y_true, y_pred,average="macro", sample_weight=None,
                  max_fpr=None, multi_class="raise", labels=None):
    fpr, tpr, thresholds = roc_auc_score(y_true, y_pred, pos_label=2)
    accuracy = auc(y_true, y_pred)
    return accuracy
"""


def matthew_correlation(y_true, y_pred, sample_weight=None):
    matthew_correlation = matthews_corrcoef(y_true, y_pred, sample_weight=sample_weight)
    return matthew_correlation


def cohen_kappa(y_true, y_pred, labels=None, weights=None, sample_weight=None):
    cohen_kappa = cohen_kappa_score(
        y_true, y_pred, labels=labels, weights=weights, sample_weight=sample_weight
    )
    return cohen_kappa


# TODO: Add ROC measurement
def roc_auc_score():
    pass


def all(y_true, y_pred):
    (
        f1_score,
        accuracy_score,
        precision_score,
        recall_score,
        matthew_corrcoef,
        cohen_kappa_score,
        obj_result,
    ) = calculate_all(y_true, y_pred)
    return obj_result  # fmin will be used


def calculate_all(y_true, y_pred):
    f1_score = f1(y_true, y_pred)
    accuracy_score = accuracy(y_true, y_pred)
    precision_score = precision(y_true, y_pred)
    recall_score = recall(y_true, y_pred)
    # auc_score = auc_score(y_true, y_pred)
    matthew_corrcoef = matthew_correlation(y_true, y_pred)
    cohen_kappa_score = cohen_kappa(y_true, y_pred)
    obj_result = (
        f1_score
        + accuracy_score
        + precision_score
        + recall_score
        + matthew_corrcoef
        + cohen_kappa_score
    )
    return (
        obj_result,
        f1_score,
        accuracy_score,
        precision_score,
        recall_score,
        matthew_corrcoef,
        cohen_kappa_score,
    )
