import warnings

warnings.simplefilter(action="ignore", category=FutureWarning)

from oddr.main.labs.optimizer import PlainOptimizer, Executor
from oddr.main.dataset.resources import Resources
from scipy.io import loadmat

if __name__ == "__main__":
    import numpy as np

    dataset_file_path = str(Resources.DATASET_DIR_PATH) + "/28seismic-bumps.mat"
    detector_name = "ABOD"
    reducer_name = "None"
    # Executor.run_experiment(dataset_file_path,PlainOptimizer.optimize_abod,PlainOptimizer.create_abod_spaces,"ABOD","None",max_eval=25)

    print(f"Extracting from {dataset_file_path}")
    dataset = loadmat(dataset_file_path)
    print(dataset["X"].shape)
    feat = 1
    for feature in dataset["X"].transpose():
        unique = np.unique(feature)
        print(feat, unique)
        feat += 1
    exit()

    print(f"Running OD:{detector_name} and Reducer:{reducer_name}...")
    X_data, dataset_name, y_true = Executor._prepare_dataset(dataset)
    result, duration = Executor.execute_optimal_result(
        (14,),
        X_data=X_data,
        y_true=y_true,
        detector_name=detector_name,
        reducer_name=reducer_name,
    )
    print(result)
    print(duration)
