FROM python:3.8-slim-buster

WORKDIR /usr/src/app
RUN pip install --upgrade pip

# install dependencies
COPY python-requirements.txt ./
RUN pip3 install --no-cache-dir -r python-requirements.txt

COPY . .
ENTRYPOINT ["python"]
