import sys
import warnings

warnings.simplefilter(action="ignore", category=FutureWarning)

from oddr.main.labs.optimizer import (
    PlainOptimizer,
    PCAOptimizer,
    TSNEOptimizer,
    UMAPOptimizer,
    Executor,
)
from oddr.main.dataset.resources import Resources
from oddr.main.labs.gdrive import MyGDrive


REDUCER_DICT = {
    "ABODNone": (PlainOptimizer.optimize_abod, PlainOptimizer.create_abod_spaces),
    "ABODPCA": (PCAOptimizer.optimize_abod, PCAOptimizer.create_abod_spaces),
    "ABODtSNE": (TSNEOptimizer.optimize_abod, TSNEOptimizer.create_abod_spaces),
    "ABODUMAP":(UMAPOptimizer.optimize_abod,UMAPOptimizer.create_abod_spaces),
    # "ABODAutoencoder":(AutoencoderOptimizer.optimize_abod,AutoencoderOptimizer.create_abod_spaces),
    "COFNone": (PlainOptimizer.optimize_cof, PlainOptimizer.create_cof_spaces),
    "COFPCA": (PCAOptimizer.optimize_cof, PCAOptimizer.create_cof_spaces),
    "COFtSNE": (TSNEOptimizer.optimize_cof, TSNEOptimizer.create_cof_spaces),
    "COFUMAP":(UMAPOptimizer.optimize_cof,UMAPOptimizer.create_cof_spaces),
    # "COFAutoencoder":(AutoencoderOptimizer.optimize_cof,AutoencoderOptimizer.create_cof_spaces),
    "COPODNone": (PlainOptimizer.run_copod, PlainOptimizer.create_copod_spaces),
    "COPODPCA": (PCAOptimizer.optimize_copod, PCAOptimizer.create_copod_spaces),
    "COPODtSNE": (TSNEOptimizer.optimize_copod, TSNEOptimizer.create_copod_spaces),
    "COPODUMAP": (UMAPOptimizer.optimize_copod,UMAPOptimizer.create_copod_spaces),
    # "COPODAutoencoder": (AutoencoderOptimizer.run_copod,AutoencoderOptimizer.create_copod_spaces),
    "IForestNone": (
        PlainOptimizer.optimize_iforest,
        PlainOptimizer.create_iforest_spaces,
    ),
    "IForestPCA": (PCAOptimizer.optimize_iforest, PCAOptimizer.create_iforest_spaces),
    "IForesttSNE": (
        TSNEOptimizer.optimize_iforest,
        TSNEOptimizer.create_iforest_spaces,
    ),
    "IForestUMAP":(UMAPOptimizer.optimize_iforest,UMAPOptimizer.create_iforest_spaces),
    # "IForestAutoencoder":(AutoencoderOptimizer.optimize_iforest,AutoencoderOptimizer.create_iforest_spaces),
    "iNNENone": (PlainOptimizer.optimize_inne, PlainOptimizer.create_inne_spaces),
    "iNNEPCA": (PCAOptimizer.optimize_inne, PCAOptimizer.create_inne_spaces),
    "iNNEtSNE": (TSNEOptimizer.optimize_inne, TSNEOptimizer.create_inne_spaces),
    "iNNEUMAP":(UMAPOptimizer.optimize_inne,UMAPOptimizer.create_inne_spaces),
    # "iNNEAutoencoder":(AutoencoderOptimizer.optimize_inne,AutoencoderOptimizer.create_inne_spaces),
    "KNNNone": (PlainOptimizer.optimize_knn, PlainOptimizer.create_knn_spaces),
    "KNNPCA": (PCAOptimizer.optimize_knn, PCAOptimizer.create_knn_spaces),
    "KNNtSNE": (TSNEOptimizer.optimize_knn, TSNEOptimizer.create_knn_spaces),
    "KNNUMAP":(UMAPOptimizer.optimize_knn,UMAPOptimizer.create_knn_spaces),
    # "KNNAutoencoder":(AutoencoderOptimizer.optimize_knn,AutoencoderOptimizer.create_knn_spaces),
    "LOFNone": (PlainOptimizer.optimize_lof, PlainOptimizer.create_lof_spaces),
    "LOFPCA": (PCAOptimizer.optimize_lof, PCAOptimizer.create_lof_spaces),
    "LOFtSNE": (TSNEOptimizer.optimize_lof, TSNEOptimizer.create_lof_spaces),
    "LOFUMAP":(UMAPOptimizer.optimize_lof,UMAPOptimizer.create_lof_spaces),
    # "LOFAutoencoder":(AutoencoderOptimizer.optimize_lof,AutoencoderOptimizer.create_lof_spaces),
}


def run(detector, reducer, dataset_file_path, max_eval):

    opt_func, spaces_func = REDUCER_DICT[f"{detector}{reducer}"]

    Executor.run_experiment(
        dataset_file_path, opt_func, spaces_func, detector, reducer, max_eval
    )

def run_all(detector,reducer,max_eval,upload,file_id,test):
    dataset_name_list = Resources.DATASET_NAME_LIST

    print(dataset_name_list)
    counter = 1
    for name in dataset_name_list:
        dataset_file_path = f"{Resources.DATASET_DIR_PATH}/{name}"
        try:
            run(detector, reducer, dataset_file_path, max_eval)
        except Exception:
            import traceback

            print(f"{name} cannot be executed")
            print(traceback.format_exc())

        if upload:
            if counter % 6 == 0 or counter == 31 or counter == 1:
                print("Uploading to Google Drive ...")
                print(f"{detector}_Report.xlsx")
                if test:
                    return
                MyGDrive.upload(f"{detector}_Report.xlsx",file_id)
        counter += 1

def run_some(detector,reducer,max_eval,upload,file_id,dataset_index,test):
    dataset_name_list = []
    for index in dataset_index:
        dataset_name_list.append(Resources.DATASET_NAME_LIST[index])
    print(dataset_name_list)

    for name in dataset_name_list:
        dataset_file_path = f"{Resources.DATASET_DIR_PATH}/{name}"
        try:
            run(detector, reducer, dataset_file_path, max_eval)
        except Exception:
            import traceback

            print(f"{name} cannot be executed")
            print(traceback.format_exc())

        if upload:
            print("Uploading to Google Drive ...")
            print(f"{detector}_Report.xlsx")
            if test:
                return
            MyGDrive.upload(f"{detector}_Report.xlsx",file_id)

def run_program(detector,reducer,max_eval,upload,file_id,dataset_index,test):
    if dataset_index == []:
        run_all(detector,reducer,max_eval,upload,file_id,test)
    else:
        run_some(detector,reducer,max_eval,upload,file_id,dataset_index,test)


if __name__ == "__main__":
    detector = str(sys.argv[1])
    reducer = str(sys.argv[2])
    max_eval = int(sys.argv[3])

    if sys.argv[4] == "False":
        upload = False
    elif sys.argv[4] == "True":
        upload = True

    file_id = str(sys.argv[5])
    dataset_index = []
    if len(sys.argv) > 6:
        for i in range(6,len(sys.argv)):
            data = int(sys.argv[i])
            dataset_index.append(data)

    print(f"Detector: {detector}")
    print(f"Reducer: {reducer}")
    print(f"MAX_EVAL: {max_eval}")
    print(f"upload: {upload}" )
    print(f"file_id: {file_id}")
    print(f"dataset={dataset_index}")
    run_program(detector,reducer,max_eval,upload,file_id,dataset_index,test=False)
