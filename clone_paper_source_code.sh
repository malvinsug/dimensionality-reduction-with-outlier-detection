#/bin/bash!

git clone https://github.com/malvinsug/principal-components-analysis.git
git clone https://github.com/malvinsug/tSNE.git
git clone https://github.com/malvinsug/umap.git
git clone https://github.com/malvinsug/keras-autoencoders.git
git clone https://github.com/malvinsug/Pypod.git
git clone https://github.com/malvinsug/scsckikit-learn.git